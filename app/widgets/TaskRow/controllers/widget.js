var args = arguments[0] || {};

var taskCollection = Alloy.Collections.task;
//getting model from collection
var taskModel = taskCollection.get(args.taskId);

function init(){
	//console.log(args);
	//set data to the widget
	$.contentLbl.setText(taskModel.get('content'));
	var imageFile = Titanium.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, taskModel.get('image'));
	if(imageFile.exists()){
		$.taskImage.setImage(imageFile.resolve());	
	}
	else{
		$.taskImage.setImage('/noImage.png');
	}
	var d = new Date(parseInt(taskModel.get('lastModified')));
	$.dateLbl.setText(d.toString());	
}

init();
