exports.definition = {
	config: {
		columns: {
		    "content": "text",
		    "lastModified": "text",
		    "status": "integer",
		    "image": "text"
		},
		adapter: {
			type: "sql",
			collection_name: "task"
		}
	},
	defaults: {
		"status": 1
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			comparator: function(task){
				//sorting by reverse lastModified
				return -task.get('lastModified');
			}
		});

		return Collection;
	}
};
