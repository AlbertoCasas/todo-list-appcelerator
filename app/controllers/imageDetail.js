var args = arguments[0] || {};

var taskCollection = Alloy.Collections.task;
//getting model from collection
var taskModel = taskCollection.get(args.taskId);

function closeView(){
	$.imageDetail.close();
}

function init(){
	var imageFile = Titanium.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, taskModel.get('image'));
	if(imageFile.exists()){
		$.taskImg.setImage(imageFile.resolve());	
	}
	else{
		$.taskImg.setImage('/noImage.png');
	}
	console.log(taskModel.get('image'));
}

init();
