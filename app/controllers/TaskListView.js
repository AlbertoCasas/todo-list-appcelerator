var args = arguments[0] || {};

//set status to the collection filter
var statusNumber = (args.status === 'pending') ? 1 :0;

function init(){
	var taskCollection = Alloy.Collections.task;
	taskCollection.sortBy('lastModified');
	taskCollection.fetch();
}

function openTaskDetail(e){
	//check if image is clicked
	if(e.source.id == "taskImage" || e.source == "[object taskImage]"){
		//open imageDetail
		Alloy.createController('imageDetail', {taskId: this.modelId}).getView().open();
	}
	else{
		//open detail screen
		Alloy.createController('TaskDetails', {taskId: this.modelId}).getView().open();	
	}
}

function whereFilter(collection){
	//filtering by status
	return collection.where({status: statusNumber});
}

init();
