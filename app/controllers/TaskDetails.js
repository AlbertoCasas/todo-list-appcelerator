var args = arguments[0] || {};
var taskCollection = Alloy.Collections.task;
var imgSrc = '/noImage.png';
var taskModel;

function closeView(){
	//close the detail window
	$.TaskDetails.close();
}

function saveTask(){
	var date = new Date();
	if(args.taskId){
		//update task
		taskModel.set({
			content: $.txtAreaContent.getValue(),
			image: imgSrc,
			lastModified: Number(date.valueOf()).toPrecision() +"",
		}).save();
		taskCollection.sortBy('lastModified');
		taskCollection.fetch();
		closeView();
	}
	else{
		//new task
		var task = Alloy.createModel('Task', {
			content: $.txtAreaContent.getValue(),
			image: imgSrc,
			lastModified: Number(date.valueOf()).toPrecision() +"",
			status: 1
		});
		task.save();
		taskCollection.add(task);
		closeView();		
	}
}

function setCompletedTask(){
	//update task as completed with status = 0
	var date = new Date();
	taskModel.set({
		content: $.txtAreaContent.getValue(),
		image: imgSrc,
		lastModified: Number(date.valueOf()).toPrecision() +"",
		status: 0
	}).save();
	taskCollection.sortBy('lastModified');
	taskCollection.fetch();
	closeView();
}

function chooseImg(){
	//show options to get the image from
	$.imageOptions.show();
}

function showShareOptions(){
	//show options to share
	$.shareOptions.show();
}

function selectShareOp(e){
	//show options depending on uer option
	if(e.index == 0){
		if(OS_ANDROID){
			//share via sms on android
			var intent = Ti.Android.createIntent({
			    action : Ti.Android.ACTION_SENDTO,
			    data : "smsto: Add users -->"
			});			 
			intent.putExtra("sms_body", taskModel.get('content') + ", status: " + taskModel.get('status'));
			Ti.Android.currentActivity.startActivity(intent);
		}
		else{
			//share via sms on ios
			var smsModule = require('com.omorandi');
			var smsDialog = smsModule.createSMSDialog();
			if(smsDialog.isSupported()){
				smsDialog.messageBody = taskModel.get('content') + ", status: "+ taskModel.get('status');
				smsDialog.open({animated: true});
			}
			else{
				alert("The required feature is not available on your device");
			}
		}
	}
	if(e.index == 1){
		//share task via email
		var emailDialog = Ti.UI.createEmailDialog();
		emailDialog.subject = "Task from TODO app";
		emailDialog.messageBody = taskModel.get('content')+ ', Status: ' + taskModel.get('status');
		var imageFile = Titanium.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, taskModel.get('image'));
		if(imageFile.exists()){
			emailDialog.addAttachment(imageFile);	
		}
		if(emailDialog.isSupported()) {
			emailDialog.open();	
		}
		else{
			alert('Please configure Email account');
		}
	}
}

function selectImgSrc(e){
	if(e.index == 0){
		//get image form camera
		Ti.Media.showCamera({
	        showControls : true,
	        animated : true,
	        error : function(_error) {
	            if (Ti.Media.isCameraSupported) {
	            	alert("Camera error: "+ ((_error.error) ? _error.error : 'code = ' + _error.code));
	            } else {
	            	alert("The required feature is not available on your device");
	            }
	        },
	        success : function(_event) {
	            var filename = new Date().getTime() + ".jpg";     
            	var imageFile = Titanium.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, filename);
	            imageFile.write(_event.media);
	            $.taskImg.setImage(imageFile.resolve());
	            imgSrc = filename;
	        },
	        saveToPhotoGallery : true,
	    });
	}
	if(e.index == 1){
		//get image from gallery
		Ti.Media.openPhotoGallery({
	        showControls : true,
	        animated : true,
	        mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO],
	        error : function(_error) {	            
	            alert("Camera error: "+ ((_error.error) ? _error.error : 'code = ' + _error.code));	            
	        },
	        success : function(_event) {
	            var filename = new Date().getTime() + ".jpg";     
            	var imageFile = Titanium.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, filename);
	            imageFile.write(_event.media);
	            $.taskImg.setImage(imageFile.resolve());
	            imgSrc = filename;
	        },
	        saveToPhotoGallery : true,
	    });
	}
}

function init(){
	//check if task is new or edited
	if(args.taskId){
		taskModel = taskCollection.get(args.taskId);
		var imageFile = Titanium.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, taskModel.get('image'));
		if(imageFile.exists()){
			$.taskImg.setImage(imageFile.resolve());	
		}
		else{
			$.taskImg.setImage('/noImage.png');
		}
		//$.taskImg.setImage(taskModel.get('image'));
		$.txtAreaContent.setValue(taskModel.get('content'));
		imgSrc = taskModel.get('image');
		if(taskModel.get('status') == 0){
			$.markBtn.setVisible(false);
			$.markBtn.setHeight(0);
		}
		$.shareBtn.setVisible(true);
	}
	else{
		$.taskImg.setImage('/noImage.png');
		$.markBtn.setVisible(false);
		$.markBtn.setHeight(0);
	}
}

init();
